import { environment } from './../../environments/environment';


import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
//import 'rxjs/Rx';


@Injectable()
export class ProductsService {

  // Variables
  http:Http;
  db:AngularFireDatabase;

  getProducts(){  
	   return this.http.get(/*"http://localhost/angular/slim/products"*/ environment.url +'/products');
  }

   getProduct(key){  
     return this.http.get(environment.url + '/products/' + key);
  }
  
  putProduct(data,key){
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    
    
    return this.http.put(/*"http://localhost/angular/slim/products/" */environment.url +'/products/' +key, data, options);
    
  }

  
  getProductsFire(){
    return this.db.list('/products').valueChanges();
  
  }

/*
 updateProduct(data,key){       
    let options = {
      headers: new Headers({
        'content-type': 'application/x-www-form-urlencoded'
      })   
    };

    let params = new HttpParams().append('name',data.name).append('price',data.price);  

    return this.http.put('http://localhost/angular/slim/products'+key, data, options);
  }
*/
  searchProducts(name){
    let params = new HttpParams().append('name', name);
    let options =  {
        headers:new Headers({
          'content-type':'application/x-www-form-urlencoded'     
        })
    } 
    return this.http.post(environment.url + '/products/search', params.toString(), options); 
  }

  constructor(http:Http, db:AngularFireDatabase) {
    this.http = http;
    this.db = db;
   }

}
