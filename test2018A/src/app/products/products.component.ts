import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ProductsService } from './products.service';


@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products;
  productsKeys;
  length_keys;
   // Instance Variables
  product_service:ProductsService;

  //Form Builder
  searchForm = new FormGroup({
  name:new FormControl()
        }); 

        
  sendData(){
    this.router.navigate(['/searchResults/' + this.searchForm.value.name]);
  }


  constructor(private service:ProductsService, private router:Router, private formBuilder:FormBuilder) { 
    this.product_service = service;
    service.getProducts().subscribe(response => {
      this.products = response.json();
      this.productsKeys = Object.keys(this.products);
    });
  }

  ngOnInit() {
  }

}


 