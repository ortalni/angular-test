//import { ProductsFormComponent } from './products/products-form/products-form.component';
import { ProductsService } from './products/products.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {RouterModule} from '@angular/router';


//FireBase
import { environment } from './../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import{ FormsModule,ReactiveFormsModule} from "@angular/forms";
import { SearchResultsComponent } from './products/search-results/search-results.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { FproductsComponent } from './fproducts/fproducts.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ProductsComponent,
    LoginComponent,
    NotFoundComponent,
    //ProductsFormComponent,
    SearchResultsComponent,
    EditProductComponent,
    FproductsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
     AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path:'navigation', component:NavigationComponent}, // localhost:4200
      {path:'login', component:LoginComponent}, // localhost:4200
      {path:'', component:ProductsComponent}, //default - localhost:4200 - homepage
      //{path:'productForm/:id', component:ProductsFormComponent},      
      {path: 'searchResults/:search_term',component:SearchResultsComponent},
       {path:'editProduct/:id', component:EditProductComponent},  
      {path:'fproducts', component:FproductsComponent},            

      {path:'**', component:NotFoundComponent} //all the routs that don't exist

    ])
  ],
  providers: [ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }