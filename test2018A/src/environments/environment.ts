// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
   production: false,
   url: 'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyCSRbh7yYlq6AnJDLB5lxN-kDGup0yAvMI",
    authDomain: "test2018-e7509.firebaseapp.com",
    databaseURL: "https://test2018-e7509.firebaseio.com",
    projectId: "test2018-e7509",
    storageBucket: "test2018-e7509.appspot.com",
    messagingSenderId: "903464095111"
  }

};